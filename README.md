# Meal Planner

This app is intended to help make planning meals easier. Users can create a list of meals that can be tagged with different categories, then set how often these tags should appear within a single plan. Meals are then randomly selected from different tag groups to create a meal plan.

## Motivation

My wife and I noticed that we didn't put much thought into planning our meals. As a result, we kept circling through the same handful of meals, eventually getting bored of them. I was taking an online university course on mobile development, and needed to come up with a final project idea. This app was written in about 4 days to solve both problems.

## Requirements

[GSon](https://github.com/google/gson)

## How to Use

The interface is pretty straight forward, but there are two things you'll need to do before you can generate a meal plan:
1. Add some meals for the generator to pick from (Main menu - Edit Meals). Be careful and consistent when adding tags as tags are case-senstitive and any typos will appear as separate tags later.
2. Set the generator settings (Main menu - Settings). You can add as many tags as you want. A tag frequency determines how many times a meal with a given tag will appear in the plan. If you set the total frequency of all tags beyond the generation limit, some meals will be randomly removed to fit the plan. If the total frequency of all tags is less than the days to generate, meals will be randomly chosen to fill in the gaps.