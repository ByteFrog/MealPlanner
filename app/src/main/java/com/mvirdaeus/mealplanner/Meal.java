package com.mvirdaeus.mealplanner;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class Meal implements Parcelable {
    public String name;
    ArrayList<String> tags;

    public Meal() {
        this.name = "";
        tags = new ArrayList<>();
    }

    public Meal(String name) {
        this.name = name;
        tags = new ArrayList<>();
    }

    public Meal(Parcel in) {
        name = in.readString();
        tags = new ArrayList<>();
        in.readStringList(this.tags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeStringList(this.tags);
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(name + " [");
        if(!tags.isEmpty()) {
            for (String tag : tags) {
                builder.append(tag);
                builder.append(", ");
            }
            builder.delete(builder.length() - 2, builder.length());
            builder.append(']');
        }
        return builder.toString();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

        @Override
        public Object createFromParcel(Parcel source) {
            return new Meal(source);
        }

        @Override
        public Object[] newArray(int size) {
            return new Meal[size];
        }
    };
}
