package com.mvirdaeus.mealplanner;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;

public class TagFrequencyDialog extends DialogFragment {

    private ITagFrequencyCreator callback_;
    private ArrayList<String> tags_;
    private Spinner tagsSpinner_;
    private EditText frequencyInput_;

    public TagFrequencyDialog(ArrayList<String> tags) {
        tags_ = tags;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_tag_frequency, null);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, tags_);
        tagsSpinner_ = view.findViewById(R.id.spinner_tags);
        tagsSpinner_.setAdapter(adapter);
        frequencyInput_ = view.findViewById(R.id.editText_frequency);

        builder.setView(view)
                .setTitle(R.string.add_tag_frequency)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int tagFrequency;
                        if(!frequencyInput_.getText().toString().isEmpty()) {
                            tagFrequency = Integer.parseInt(frequencyInput_.getText().toString());
                        }
                        else {
                            tagFrequency = -1;
                        }
                        int selectedIndex = tagsSpinner_.getSelectedItemPosition();
                        TagFrequency frequency = new TagFrequency(tags_.get(selectedIndex), tagFrequency);
                        callback_.onTagFrequencyCreated(frequency);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            callback_ = (ITagFrequencyCreator) context;
        }
        catch(ClassCastException e) {
            Log.d("TAG", "Activity doesn't implement the ITagFrequencyCreator interface.");
        }
    }
}
