package com.mvirdaeus.mealplanner;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class AboutDialog extends DialogFragment {

    TextView versionView_;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

        LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_about, null);

        versionView_ = view.findViewById(R.id.textView_version);
        versionView_.setText(getString(R.string.version, BuildConfig.VERSION_NAME));

        TextView reportBugView = view.findViewById(R.id.textView_reportBug);
        reportBugView.setMovementMethod(LinkMovementMethod.getInstance());

        builder.setView(view);
        return builder.create();
    }
}
