package com.mvirdaeus.mealplanner;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class TagFrequency implements Parcelable {
    public String tag;
    public int frequency;

    public TagFrequency() {
        tag = "";
        frequency = 0;
    }

    public TagFrequency(String tag, int frequency) {
        this.tag = tag;
        this.frequency = frequency;
    }

    public TagFrequency(Parcel in) {
        tag = in.readString();
        frequency = in.readInt();
    }

    public static final Creator<TagFrequency> CREATOR = new Creator<TagFrequency>() {
        @Override
        public TagFrequency createFromParcel(Parcel in) {
            return new TagFrequency(in);
        }

        @Override
        public TagFrequency[] newArray(int size) {
            return new TagFrequency[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tag);
        dest.writeInt(frequency);
    }

    @NonNull
    @Override
    public String toString() {
        return (tag + ": " + frequency);
    }


}
