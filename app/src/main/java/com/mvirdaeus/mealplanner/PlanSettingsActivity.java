package com.mvirdaeus.mealplanner;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class PlanSettingsActivity
        extends AppCompatActivity
        implements ITagFrequencyCreator {

    private ArrayList<TagFrequency> tagFrequencies_;
    private ArrayList<String> tags_;

    private EditText daysEntry_;
    private ListView tagFrequenciesView_;
    private ArrayAdapter<TagFrequency> tagFrequenciesAdapter_;

    private View.OnClickListener addTagListener_ = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!tags_.isEmpty()) {
                showTagFrequencyDialog();
            }
            else {
                Toast.makeText(getBaseContext(), R.string.no_tags_error, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private AdapterView.OnItemClickListener removeTagFrequencyListener_ = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            removeTagFrequency(position);
        }
    };

    private View.OnClickListener saveCloseListener_ = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();

            int days = Integer.parseInt(daysEntry_.getText().toString());
            intent.putExtra(MainActivity.EXTRA_DAYS, days);

            intent.putParcelableArrayListExtra(MainActivity.EXTRA_FREQUENCY_LIST, tagFrequencies_);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_settings);

        daysEntry_ = findViewById(R.id.editText_days);
        tagFrequenciesView_ = findViewById(R.id.listView_tagFrequencies);

        if(savedInstanceState == null) {
            Intent intent = getIntent();
            daysEntry_.setText(String.valueOf(intent.getIntExtra(MainActivity.EXTRA_DAYS, 7)));
            tags_ = intent.getStringArrayListExtra(MainActivity.EXTRA_TAG_LIST);
            tagFrequencies_ = intent.getParcelableArrayListExtra(MainActivity.EXTRA_FREQUENCY_LIST);
            for(TagFrequency frequency : tagFrequencies_) {
                tags_.remove(frequency.tag);
            }
        }
        else {
            daysEntry_.setText(String.valueOf(savedInstanceState.getInt(MainActivity.EXTRA_DAYS)));
            tags_ = savedInstanceState.getStringArrayList(MainActivity.EXTRA_TAG_LIST);
            tagFrequencies_ = savedInstanceState.getParcelableArrayList(MainActivity.EXTRA_FREQUENCY_LIST);
        }

        tagFrequenciesAdapter_ = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, tagFrequencies_);
        tagFrequenciesView_.setAdapter(tagFrequenciesAdapter_);
        tagFrequenciesView_.setOnItemClickListener(removeTagFrequencyListener_);

        findViewById(R.id.button_addTag).setOnClickListener(addTagListener_);
        findViewById(R.id.button_saveClose).setOnClickListener(saveCloseListener_);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList(MainActivity.EXTRA_FREQUENCY_LIST, tagFrequencies_);
        outState.putStringArrayList(MainActivity.EXTRA_TAG_LIST, tags_);
        outState.putInt(MainActivity.EXTRA_DAYS, Integer.parseInt(daysEntry_.getText().toString()));
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onTagFrequencyCreated(TagFrequency value) {
        if(value.frequency > 0) {
            addTagFrequency(value);
        }
        else {
            Toast.makeText(this, R.string.blank_frequency_error, Toast.LENGTH_SHORT).show();
        }
    }

    private void addTagFrequency(TagFrequency value) {
        tagFrequencies_.add(value);
        tags_.remove(value.tag);
        tagFrequenciesAdapter_.notifyDataSetChanged();
    }

    private void removeTagFrequency(int index) {
        tags_.add(tagFrequencies_.get(index).tag);
        tagFrequencies_.remove(index);
        tagFrequenciesAdapter_.notifyDataSetChanged();
    }

    private void showTagFrequencyDialog() {
        DialogFragment dialog = new TagFrequencyDialog(tags_);
        dialog.show(getSupportFragmentManager(), "TagFrequencyDialog");
    }
}