package com.mvirdaeus.mealplanner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;


public class MainActivity extends AppCompatActivity {

    //Actions
    public static final String ACTION_NEW_MEAL = "com.mvirdaeus.action.NEW_MEAL";
    public static final String ACTION_EDIT_MEAL = "com.mvirdaeus.action.EDIT_MEAL";
    public static final String ACTION_DELETE_MEAL = "com.mvirdaeus.action.DELETE_MEAL";

    //Requests
    public static final int REQUEST_MEAL_LIST = 1;
    public static final int REQUEST_SETTINGS = 2;

    //Labels for Extras
    public static final String EXTRA_MEAL_LIST = "MEAL_LIST";
    public static final String EXTRA_MEAL = "MEAL";
    public static final String EXTRA_LIST_POS = "LIST_POS";
    public static final String EXTRA_DAYS = "DAYS";
    public static final String EXTRA_TAG_LIST = "TAG_LIST";
    public static final String EXTRA_FREQUENCY_LIST = "FREQUENCY_LIST";
    public static final String EXTRA_MEAL_PLAN = "MEAL_PLAN";

    private ArrayList<Meal> mealList_;
    private int days_ = 7;
    private ArrayList<TagFrequency> tagFrequencies_;
    private ArrayList<String> mealPlan_;
    private ListView mealPlanView_;
    private ArrayAdapter<String> mealPlanAdapter_;

    private View.OnClickListener generatePlanListener_ = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            generatePlan();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        if(savedInstanceState == null) {
            Gson gson = new Gson();
            SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);

            String mealList = preferences.getString(EXTRA_MEAL_LIST, null);
            if(mealList == null) {
                mealList_ = new ArrayList<>();
            }
            else {
                mealList_ = gson.fromJson(mealList, new TypeToken<ArrayList<Meal>>(){}.getType());
            }

            String tagFrequencies = preferences.getString(EXTRA_FREQUENCY_LIST, null);
            if(tagFrequencies == null) {
                tagFrequencies_ = new ArrayList<>();
            }
            else {
                tagFrequencies_ = gson.fromJson(tagFrequencies, new TypeToken<ArrayList<TagFrequency>>(){}.getType());
            }

            String mealPlan = preferences.getString(EXTRA_MEAL_PLAN, null);
            if(mealPlan == null) {
                mealPlan_ = new ArrayList<>();
            }
            else {
                mealPlan_ = gson.fromJson(mealPlan, new TypeToken<ArrayList<String>>(){}.getType());
            }

            int days = preferences.getInt(EXTRA_DAYS, 7);
            if(days != 7) {
                days_ = days;
            }

        }
        else {
            mealList_ = savedInstanceState.getParcelableArrayList(EXTRA_MEAL_LIST);
            tagFrequencies_ = savedInstanceState.getParcelableArrayList(EXTRA_FREQUENCY_LIST);
            mealPlan_ = savedInstanceState.getStringArrayList(EXTRA_MEAL_PLAN);
        }

        mealPlanAdapter_ = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mealPlan_);
        mealPlanView_ = findViewById(R.id.listView_mealPlan);
        mealPlanView_.setAdapter(mealPlanAdapter_);

        findViewById(R.id.fab_generate).setOnClickListener(generatePlanListener_);

        //DEBUG
        /*Meal meal = new Meal("Sloppy joes");
        meal.tags.add("ground round");
        mealList_.add(meal);
        meal = new Meal("Hamburger pasta");
        meal.tags.add("ground round");
        meal.tags.add("pasta");
        mealList_.add(meal);
        meal = new Meal("Classic pizza");
        meal.tags.add("pizza");
        mealList_.add(meal);
        meal = new Meal("Ranch pizza");
        meal.tags.add("pizza");
        mealList_.add(meal);
        meal = new Meal("Sweet chili pizza");
        meal.tags.add("pizza");
        mealList_.add(meal);
        meal = new Meal("Enchiladas");
        meal.tags.add("beans");
        mealList_.add(meal);
        meal = new Meal("Perogies");
        meal.tags.add("easy");
        mealList_.add(meal);
        meal = new Meal("Mac & cheese");
        meal.tags.add("easy");
        meal.tags.add("pasta");
        mealList_.add(meal);
        meal = new Meal("Mashed potatoes");
        meal.tags.add("potato");
        mealList_.add(meal);
        meal = new Meal("Scalloped potatoes");
        meal.tags.add("potato");
        mealList_.add(meal);*/
    }

    @Override
    protected void onPause() {
        /*  NOTE: If this were a proper app, I absolutely wouldn't store the list of meals in a preferences file since it could
        potentially have hundreds of items. However, since this is just a demonstration of skills acquired through the course
        and the app won't actually be released, I've stored the meals list just to show how to use the preferences file.
        Gson is used to convert lists into strings for storage.
        */
        SharedPreferences preferences = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Gson gson = new Gson();
        String mealList = gson.toJson(mealList_);
        editor.putString(EXTRA_MEAL_LIST, mealList);
        String tagFrequencies = gson.toJson(tagFrequencies_);
        editor.putString(EXTRA_FREQUENCY_LIST, tagFrequencies);
        String mealPlan = gson.toJson(mealPlan_);
        editor.putString(EXTRA_MEAL_PLAN, mealPlan);

        editor.putInt(EXTRA_DAYS, days_);

        editor.apply();

        super.onPause();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch(requestCode) {
            case MainActivity.REQUEST_MEAL_LIST:
                if(resultCode == MainActivity.RESULT_OK) {
                    mealList_ = data.getParcelableArrayListExtra(EXTRA_MEAL_LIST);
                }
                break;
            case MainActivity.REQUEST_SETTINGS:
                if(resultCode == MainActivity.RESULT_OK) {
                    days_ = data.getIntExtra(EXTRA_DAYS, 7);
                    tagFrequencies_ = data.getParcelableArrayListExtra(EXTRA_FREQUENCY_LIST);
                }
                break;
            default:
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();

        switch(id) {
            case R.id.action_editMeals: {
                Intent intent = new Intent(this, MealListActivity.class);
                intent.putParcelableArrayListExtra(EXTRA_MEAL_LIST, mealList_);
                startActivityForResult(intent, REQUEST_MEAL_LIST);
            }
                break;
            case R.id.action_settings: {
                Intent intent = new Intent(this, PlanSettingsActivity.class);
                intent.putExtra(EXTRA_DAYS, days_);
                intent.putStringArrayListExtra(EXTRA_TAG_LIST, getTags());
                intent.putParcelableArrayListExtra(EXTRA_FREQUENCY_LIST, tagFrequencies_);
                startActivityForResult(intent, REQUEST_SETTINGS);
            }
                break;
            case R.id.action_about: {
                DialogFragment dialog = new AboutDialog();
                dialog.show(getSupportFragmentManager(), "AboutDialog");
            }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<String> getTags() {
        ArrayList<String> tags = new ArrayList<>();
        for(Meal meal : mealList_) {
            for(String tag : meal.tags) {
                if(!tags.contains(tag)) {
                    tags.add(tag);
                }
            }
        }
        return tags;
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList(EXTRA_MEAL_LIST, mealList_);
        outState.putParcelableArrayList(EXTRA_FREQUENCY_LIST, tagFrequencies_);
        outState.putStringArrayList(EXTRA_MEAL_PLAN, mealPlan_);
        super.onSaveInstanceState(outState);
    }

    private void generatePlan() {
        new CreatePlanTask(this).execute();
    }

    private class CreatePlanTask extends AsyncTask<Void, Integer, ArrayList<String>> {

        private WeakReference<MainActivity> activityReference_;

        CreatePlanTask(MainActivity context) {
            activityReference_ = new WeakReference<>(context);
        }

        @Override
        protected ArrayList<String> doInBackground(Void... voids) {
            /* The simplest process for building a meal plan is to start with a master list of meals to choose
               from. For each tag in the frequency list, create a sub-list of meals containing that tag and select
               the required number of meals from it. Each time a meal is chosen, remove it from both the sub-list
               and the master list. This ensures that the same meal isn't chosen twice by tag or over the whole plan.
             */
            ArrayList<String> mealPlan = new ArrayList<>();
            ArrayList<Meal> masterList = (ArrayList<Meal>) mealList_.clone();
            ArrayList<TagFrequency> tagFrequencies = (ArrayList<TagFrequency>) tagFrequencies_.clone();
            Random random = new Random();

            //for(TagFrequency tagFrequency : tagFrequencies_) {
            /*  Build the meal plan from frequencies chosen at random because any unused meals with a tag are
                removed to limit the number of meals with a given tag to that meal's frequency.
             */
            while(!tagFrequencies.isEmpty()) {
                TagFrequency tagFrequency = tagFrequencies.remove(random.nextInt(tagFrequencies.size()));
                ArrayList<String> taggedMealList = new ArrayList<>();

                //Find all meals from the master list and add them to the tagged meal list.
                for(Meal meal : masterList) {
                    if(meal.tags.contains(tagFrequency.tag)) {
                        taggedMealList.add(meal.name);
                    }
                }

                //Select the required number of meals with this tag from the list.
                for(int i = 0; i < tagFrequency.frequency && taggedMealList.size() > 0; ++i) {
                    int index = random.nextInt(taggedMealList.size());
                    String selectedMeal = taggedMealList.get(index);
                    //Add the meal to the meal plan and remove if from the other lists.
                    mealPlan.add(selectedMeal);
                    taggedMealList.remove(index);
                    Iterator<Meal> iterator = masterList.iterator();
                    while(iterator.hasNext()) {
                        Meal curMeal = iterator.next();
                        if(curMeal.name.equals(selectedMeal)) {
                            iterator.remove();
                            break;
                        }
                    }
                }

                /*  Remove all unused meals with the current tag from the master list. This ensures that a meal
                    with any specified tag will not appear on the meal list more than its frequency allows.
                 */
                for(String selectedMeal : taggedMealList) {
                    Iterator<Meal> iterator = masterList.iterator();
                    while(iterator.hasNext()) {
                        Meal curMeal = iterator.next();
                        if(curMeal.name.equals(selectedMeal)) {
                            iterator.remove();
                            break;
                        }
                    }
                }
            }

            //If there are spaces left in the plan, fill them by picking randomly from the meals left on the master list.
            while(mealPlan.size() < days_ && masterList.size() > 0) {
                int index = random.nextInt(masterList.size());
                mealPlan.add(masterList.get(index).name);
                masterList.remove(index);
            }

            /* After generating the meal plan, make sure that the total number of meals chosen by frequency
               doesn't exceed the number of meals to generate. If so, randomly remove meals until it matches
               and display a warning. If the list is too small, pop up a warning that there weren't enough
               meals to create the plan.
             */
            if(mealPlan.size() < days_) {
                publishProgress(-1);
            }
            else if(mealPlan.size() > days_) {
                while(mealPlan.size() > days_) {
                    mealPlan.remove(random.nextInt(mealPlan.size()));
                }
                publishProgress(1);
            }

            return mealPlan;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            MainActivity activity = activityReference_.get();
            if(activity == null || activity.isFinishing()) { return; }

            switch(values[0]) {
                case -1:
                    Toast.makeText(activity, R.string.not_enough_meals_warning, Toast.LENGTH_SHORT).show();
                    break;
                case 0:
                    break;
                case 1:
                    Toast.makeText(activity, R.string.too_many_meals_warning, Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<String> mealPlan) {
            MainActivity activity = activityReference_.get();
            if(activity == null || activity.isFinishing()) { return; }

            mealPlan_ = mealPlan;
            mealPlanAdapter_ = new ArrayAdapter<>(activity, android.R.layout.simple_list_item_1, mealPlan_);
            mealPlanView_.setAdapter(mealPlanAdapter_);
        }
    }
}