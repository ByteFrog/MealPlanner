package com.mvirdaeus.mealplanner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipDrawable;
import com.google.android.material.chip.ChipGroup;

public class EditMealActivity extends AppCompatActivity {

    Meal meal_;
    EditText mealName_;
    ListView tagsList_;
    ArrayAdapter<String> tagsAdapter_;
    boolean isNewMeal_ = false;

    AdapterView.OnItemClickListener deleteTagListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            meal_.tags.remove(position);
            tagsAdapter_.notifyDataSetChanged();
        }
    };

    View.OnClickListener saveCloseClickedListener_ = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            meal_.name = mealName_.getText().toString();
            Intent intent = new Intent();
            intent.putExtra(MainActivity.EXTRA_MEAL, meal_);
            int listPos = getIntent().getIntExtra(MainActivity.EXTRA_LIST_POS, -1);
            intent.putExtra(MainActivity.EXTRA_LIST_POS, listPos);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    };

    View.OnClickListener addTagClickedListener_ = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showNewTagDialog();
        }
    };

    View.OnClickListener deleteMealListener_ = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.ACTION_DELETE_MEAL);
            int listPos = getIntent().getIntExtra(MainActivity.EXTRA_LIST_POS, -1);
            intent.putExtra(MainActivity.EXTRA_LIST_POS, listPos);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_meal);

        mealName_ = findViewById(R.id.editText_mealName);
        tagsList_ = findViewById(R.id.listView_tags);

        findViewById(R.id.button_saveClose).setOnClickListener(saveCloseClickedListener_);
        findViewById(R.id.button_addTag).setOnClickListener(addTagClickedListener_);
        findViewById(R.id.button_deleteMeal).setOnClickListener(deleteMealListener_);

        Intent intent = getIntent();
        if(intent.getAction().equals(MainActivity.ACTION_EDIT_MEAL)) {
            meal_ = intent.getParcelableExtra(MainActivity.EXTRA_MEAL);
            mealName_.setText(meal_.name);
            isNewMeal_ = false;
        }
        else {
            meal_ = new Meal();
            isNewMeal_ = true;
        }
        tagsAdapter_ = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, meal_.tags);
        tagsList_.setAdapter(tagsAdapter_);
        tagsList_.setOnItemClickListener(deleteTagListener);
    }

    private void showNewTagDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.add_new_tag);

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String tag = input.getText().toString();
                if(!tag.isEmpty()) {
                    if(!meal_.tags.contains(tag)) {
                        meal_.tags.add(tag);
                        tagsAdapter_.notifyDataSetChanged();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), R.string.duplicate_tag_error, Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(getApplicationContext(), R.string.blank_tag_error, Toast.LENGTH_SHORT).show();
                }
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}