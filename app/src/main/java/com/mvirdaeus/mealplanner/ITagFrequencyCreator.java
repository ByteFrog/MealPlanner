package com.mvirdaeus.mealplanner;

public interface ITagFrequencyCreator {
    void onTagFrequencyCreated(TagFrequency value);
}
