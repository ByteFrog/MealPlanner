package com.mvirdaeus.mealplanner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.util.ArrayList;

public class MealListActivity extends AppCompatActivity {

    public static final int REQUEST_MEAL = 1;

    private ListView mealListView_;
    private ArrayList<Meal>  mealList_;
    private ArrayAdapter<Meal> mealListAdapter_;

    private BroadcastReceiver receiver_ = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(MainActivity.ACTION_DELETE_MEAL)) {
                int listPos = intent.getIntExtra(MainActivity.EXTRA_LIST_POS, -1);
                if(listPos != -1) {
                    mealList_.remove(listPos);
                    mealListAdapter_.notifyDataSetChanged();
                }
            }
        }
    };

    View.OnClickListener newMealListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getBaseContext(), EditMealActivity.class);
            intent.setAction(MainActivity.ACTION_NEW_MEAL);
            startActivityForResult(intent, REQUEST_MEAL);
        }
    };

    AdapterView.OnItemClickListener editMealListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Meal meal = mealList_.get(position);

            Intent intent = new Intent(getBaseContext(), EditMealActivity.class);
            intent.setAction(MainActivity.ACTION_EDIT_MEAL);
            intent.putExtra(MainActivity.EXTRA_MEAL, meal);
            intent.putExtra(MainActivity.EXTRA_LIST_POS, position);
            startActivityForResult(intent, REQUEST_MEAL);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_list);

        if(savedInstanceState == null) {
            Intent intent = getIntent();
            mealList_ = intent.getParcelableArrayListExtra(MainActivity.EXTRA_MEAL_LIST);
        }
        else {
            mealList_ = savedInstanceState.getParcelableArrayList(MainActivity.EXTRA_MEAL_LIST);
        }

        mealListAdapter_ = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mealList_);

        mealListView_ = findViewById(R.id.listview_meals);
        mealListView_.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        mealListView_.setAdapter(mealListAdapter_);
        mealListView_.setOnItemClickListener(editMealListener);

        FloatingActionButton addMealButton = findViewById(R.id.fab_addMeal);
        addMealButton.setOnClickListener(newMealListener);

        LocalBroadcastManager.getInstance(this).registerReceiver(receiver_, new IntentFilter(MainActivity.ACTION_DELETE_MEAL));
    }

    @Override
    protected void onDestroy() {
        if(receiver_ != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver_);
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(MainActivity.EXTRA_MEAL_LIST, mealList_);
        setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_MEAL) {
            if(resultCode == Activity.RESULT_OK) {
                Meal meal = data.getParcelableExtra(MainActivity.EXTRA_MEAL);
                if(!meal.name.isEmpty()) {
                        int listPos = data.getIntExtra(MainActivity.EXTRA_LIST_POS, -1);
                        if (listPos == -1) {
                            if(!mealExists(meal)) {
                                mealList_.add(meal);
                            }
                            else {
                                Toast.makeText(this, R.string.duplicate_meal_error, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            mealList_.set(listPos, meal);
                        }
                        mealListAdapter_.notifyDataSetChanged();
                }
                else {
                    Toast.makeText(this, R.string.blank_meal_name_error, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList(MainActivity.EXTRA_MEAL_LIST, mealList_);
        super.onSaveInstanceState(outState);
    }

    private boolean mealExists(Meal meal) {
        boolean value = false;
        for(Meal m : mealList_) {
            if(m.name.equals(meal.name)) {
                value = true;
                break;
            }
        }
        return value;
    }
}